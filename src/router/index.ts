import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import ProfilRestaurantView from '../views/ProfilRestaurantView.vue'
import CreateRestaurantView from '../views/CreateRestaurantView.vue'
import CreateMenuView from '../views/CreateMenuView.vue'
import RestaurantView from '../views/RestaurantView.vue'
import DetailMenuView from '../views/DetailMenuView.vue'
import CommandsView from '../views/CommandsView.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/restaurant',
    name: 'home',
    component: RestaurantView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/register-restaurant',
    name: 'register-restaurant',
    component: CreateRestaurantView
  },
  {
    path: '/create-menu',
    component: CreateMenuView
  },
  {
    path: '/detail-menu',
    name: 'detail-menu',
    component: DetailMenuView
  },
  {
    path: '/commands',
    name: 'commands',
    component: CommandsView
  },
  {
    path: '/profil-restaurant',
    name: 'profil-restaurant',
    component: ProfilRestaurantView
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
