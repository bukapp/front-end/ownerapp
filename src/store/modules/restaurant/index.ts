
import router from '@/router';
import axios from 'axios';


const state = {
  RestaurantsList: [],
  SelectedRestaurant: {},
  MyRestaurant: {},
  hasRestaurant: false,
}

const mutations = {
  UPDATE_RESTAURANTS_LIST(state: any, payload: any) {
    state.RestaurantsList = payload;
  },
  UPDATE_MY_RESTAURANT(state: any, payload: any) {
    state.MyRestaurant = payload;
  },
  UPDATE_SELECTED_RESTAURANT(state: any, payload: any) {
    state.SelectedRestaurant = payload;
  },
  UPDATE_HASRESTAURANT(state: any, payload: any) {
    state.hasRestaurant = payload;
  },
}


const actions = {
  getRestaurantsList({ commit }: any) {
    axios.get(`/gateway/restaurants/`).then((response) => {
      commit('UPDATE_RESTAURANTS_LIST', response.data)
    });
  },
  getRestaurant({ commit }: any, id: any) {
    axios.get(`/gateway/restaurants/user/${id}`).then((response) => {
      commit('UPDATE_MY_RESTAURANT', JSON.parse(JSON.stringify(response.data)))
      commit('UPDATE_HASRESTAURANT', true)

      localStorage.restaurant = JSON.stringify(response.data)
      localStorage.hasRestaurant = true

      router.push('/')
    }).catch(() => {
      router.push('/register-restaurant')
    }
    );
  },
  addRestaurant({ commit }: any, restaurant: any) {
    axios.post('/gateway/restaurants/', restaurant).then((response) => {
      commit('UPDATE_RESTAURANTS_LIST', JSON.parse(JSON.stringify(response.data)))
      commit('UPDATE_HASRESTAURANT', true)

      localStorage.restaurant = JSON.stringify(response.data)
      localStorage.hasRestaurant = true

      router.push('/')
    });
  },
  selectRestaurant({ commit }: any, restaurant: any) { 
    commit('UPDATE_SELECTED_RESTAURANT', restaurant)
  },
}

const getters = {
  restaurants: (state: any) => state.RestaurantsList,
  restaurant: (state: any) => state.SelectedRestaurant,
  hasRestaurant: (state: any) => state.hasRestaurant,
}

const restaurantModule = {
  state,
  mutations,
  actions,
  getters
}

export default restaurantModule;