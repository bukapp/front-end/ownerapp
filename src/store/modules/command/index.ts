
import router from '@/router';
import axios from 'axios';

const state = {
    CommandsList: []
}

const mutations = {
    UPDATE_COMMANDS_LIST(state: any, payload: any) {
        state.CommandsList = payload;
      },
}


const actions = {
    newCommand({ commit }: any, cart: any) {
        axios.post('/gateway/commands/', cart).then((response) => {
            console.log(response.data)
        });
    },
    getCommandsByRestaurant({ commit }: any, id: any) {
        axios.get(`/gateway/commands/restaurant/${id}`).then((response) => {
            commit('UPDATE_COMMANDS_LIST', response.data)
        });
    },
    validateCommand({ commit }: any, id: any) {
        axios.get(`/gateway/commands/validate/${id}`).then((response) => {
            console.log(response.data)
            alert("Commande acceptée")
        });
    },
}

const getters = {
    commands: (state: any) => state.CommandsList,
}

const commandModule = {
    state,
    mutations,
    actions,
    getters
}

export default commandModule;