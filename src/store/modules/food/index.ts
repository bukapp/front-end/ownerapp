
import router from '@/router';
import axios from 'axios';

const state = {
  FoodsList: [],
  SelectedFood: {},
}

const mutations = {
  UPDATE_FOODS_LIST(state: any, payload: any) {
    state.FoodsList = payload;
  },
  UPDATE_SELECTED_FOOD(state: any, payload: any) {
    state.SelectedFood = payload;
  },
}


const actions = {
  getFoodsList({ commit }: any) {
    axios.get(`/gateway/foods/`).then((response) => {
      commit('UPDATE_FOODS_LIST', response.data)
    });
  },
  getFoodsByRestaurant({ commit }: any, id: any) {
    axios.get(`/gateway/foods/restaurant/${id}`).then((response) => {
      commit('UPDATE_FOODS_LIST', response.data)
    });
  },
  addMenu({ commit }: any, food: any) {
    axios.post('/gateway/foods/', food).then((response) => {
      commit('UPDATE_FOODS_LIST', response.data)
      router.push('restaurant')
    });
  },
  deleteMenu({ commit }: any, id: any) {
    axios.delete(`/gateway/foods/${id}`).then((response) => {
      router.push('restaurant')
    });
  },
  selectFood({ commit }: any, food: any) {
    commit('UPDATE_SELECTED_FOOD', food)
  },
}

const getters = {
  foods: (state: any) => state.FoodsList,
  food: (state: any) => state.SelectedFood,
  foodById: (state: any) => (id: any) => {
    return state.FoodsList.find((FoodsList: { id: any; }) => FoodsList.id === id)
  }
}

const foodModule = {
  state,
  mutations,
  actions,
  getters
}

export default foodModule;