import Vue from 'vue'
import Vuex from 'vuex'
import restaurant from './modules/restaurant'
import food from './modules/food'
import user from './modules/user'
import command from './modules/command'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    restaurant,
    food,
    user,
    command
  }
})